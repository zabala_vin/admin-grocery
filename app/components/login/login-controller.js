angular.module('login-controller',[])

.controller('loginCtrl', function ($scope, $rootScope, $log, $http, $state, $cookies, $q, Authorization) {

  console.log($rootScope.testData);

  $scope.forgot = true;
  $scope.login = false;
  $scope.password = "Forgot password ?";
  $scope.user = "";

  $scope.alerts = [];

  $scope.switch = function () {
    $scope.forgot = !$scope.forgot;
    $scope.login = !$scope.login;
    if ($scope.password == "Forgot password ?")
      $scope.password = "Log In";
    else
      $scope.password = "Forgot password ?";
  }

  $scope.log = function (user) {
  	$scope.user = user;

    $http.get('assets/data/database.json')
    .then(function(response) {
      if (typeof response.data === 'object') {
        $scope.data = response.data;
        if ($scope.user.name !== "" && $scope.user.password !== "") {
          //if ($scope.user.name == $scope.data.account[0].username && $scope.user.password == $scope.data.account[0].password) {
          if ($scope.user.name == $rootScope.testData.account.email && $scope.user.password == $rootScope.testData.account.password) {
            Authorization.go('app');
            $cookies.put('username', $scope.user.name);
            $cookies.put('password', $scope.user.password);
            $cookies.put('Authorization', true);
            console.log($cookies.getAll());
          }
          else if ($scope.user.name == $scope.data.account[0].username || $scope.user.password == $scope.data.account[0].password) {
            $scope.alerts.push({type: 'danger', msg: 'Invalid Username or Password'});
          }
          else if ($scope.user.name != $scope.data.account[0].username && $scope.user.password != $scope.data.account[0].password) {
            $scope.alerts.push({type: 'danger', msg: 'Account Not found'});
          }
        }
      } else {
          // invalid response
      }

    }, function(response) {
      // something went wrong
    });
  }
})