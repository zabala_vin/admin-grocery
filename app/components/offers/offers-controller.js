angular.module('offers-controller',[])

.controller('offersCtrl', function ($scope, $rootScope, $uibModal, $log, $sce, $http, $state, $stateParams) {

  function init () {

    // Run this code only once
    if (!$stateParams.filter) {
      $state.go('offers', { filter: "All" });

      function http () {
        $http.get('assets/data/database.json')
          .then(function(response) {
            if (response.status == 200) {
              $rootScope.offers = response.data.restaurant[0].menu;
            } else {
                // invalid response
            }
          }, function(response) {
            // something went wrong
        });
      }
      http ();
      console.log("Offer Initialize"); 
    }
    // Run this code every Reload/Refresh
    if ($state.current.name == 'offers') {
      $rootScope.sidebar_switch = true;
      $scope.ismeridian = true;

      $rootScope.dashboard = [ ];
      $rootScope.dashboard = [
        {"name" : "All"}, 
        {"name" : "Main Course"}, 
        {"name" : "Breakfast"}, 
        {"name" : "Side Dish"}, 
        {"name" : "Drinks"}, 
        {"name" : "Special"}
      ];
    }
  };

  if($stateParams.filter == "All") 
    $scope.filter = "";
  else
    $scope.filter = $stateParams.filter;

  // Modal Function and Controller
  $scope.newOffer    = function () {

    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/offers/offersNew-modal.html',
      controller: 'newOffer',
      backdrop: 'static'
    });
  };
  $scope.editOffer   = function (index) {

    $rootScope.index = index;
    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/offers/offersEdit-modal.html',
      controller: 'editOffer',
      backdrop: 'static'
    });
  };
  $scope.deleteOffer = function (index) {
    $rootScope.index = index;
    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/offers/offersDelete-modal.html',
      controller: 'deleteOffer',
      backdrop: 'static',
      size: 'sm'
    });
  };
  $scope.newCategory = function () {

    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/offers/offersCategory-modal.html',
      controller: 'categoryOffer',
      backdrop: 'static',
      size: 'sm'
    });
  };
  init ();
})

.controller('newOffer',      function ($scope, $rootScope, $uibModalInstance, $http, $filter) {

  $scope.from = new Date(0,0,0,08,0);
  $scope.to   = new Date(0,0,0,08,0);

  $scope.addOn        = [ ];
  $scope.ismeridian   = true;
  $rootScope.category = "";
  $scope.setting      = false;

  $scope.categories = function (item) {
    $rootScope.category = item;
  }
  $scope.ok = function (offer) {
    console.log(offer.photo);
    if (offer.name && offer.price && offer.desc && ( $scope.to > $scope.from )) {

      $rootScope.offers.push({ name    : offer.name, 
                               price   : offer.price, 
                               avaTo   : $scope.to.getTime(),  
                               avaFrom : $scope.from.getTime(), 
                               desc    : offer.desc, 
                               dish    : $rootScope.category });

      console.log($scope.from + " - " + $scope.to);

      $uibModalInstance.dismiss('cancel');
    }
  }
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('editOffer',     function ($scope, $rootScope, $uibModalInstance, $http, $filter, $stateParams) {

  $scope.filter = $stateParams.filter;
  $rootScope.offer = $rootScope.offers;

  $http.get('assets/data/database.json')
    .then(function(response) {
      if (typeof response.data === 'object') {
        $scope.offer = response.data.restaurant[0].menu;
        $scope.avaFrom = new Date(0,0,0, ($filter('date')($scope.offer[$rootScope.index].avaFrom, 'h')), ($filter('date')($scope.offer[$rootScope.index].avaFrom, 'mm')) ,0);
        $scope.avaTo   = new Date(0,0,0, ($filter('date')($scope.offer[$rootScope.index].avaTo, 'h')),   ($filter('date')($scope.offer[$rootScope.index].avaTo, 'mm')) ,0);     
      } else {
          // invalid response
      }

    }, function(response) {
      // something went wrong
  });

  $scope.ok = function (offer, index, start, end) {
    if (offer.name && offer.price && offer.desc && offer.dish && offer.pic && ($filter('date')(end, 'h') > $filter('date')(start, 'h'))) {
      console.log(offer);
      if (offer) { 
        $rootScope.offers.splice(index, 1, offer);
      }     
      $uibModalInstance.dismiss('cancel');
    }
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})
.controller('categoryOffer', function ($scope, $rootScope, $uibModalInstance) {

  $scope.ok = function (newcategory) {
    if (newcategory) {
      console.log(newcategory);
      $rootScope.dashboard.push({ "name":newcategory });
      $uibModalInstance.dismiss('cancel');
    }
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})
.controller('deleteOffer',   function ($scope, $rootScope, $uibModalInstance, $http, $stateParams) {

  $scope.filter = $stateParams.filter;
  console.log("filter = " + $scope.filter);

  $scope.ok = function (index) {
    console.log("index = " + index);
    if (index >= 0) {
      $rootScope.offers.splice(index, 1);
      console.log($rootScope.offers);
    }
    $uibModalInstance.dismiss('cancel');
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})