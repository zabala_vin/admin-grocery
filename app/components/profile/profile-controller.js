angular.module('profile-controller', [])

.controller('profileCtrl', function ($scope, $log, $http, $window) {
	
	$scope.profile = [];
  $scope.account = [];
  $scope.profile = {};
	$scope.isProfile = false;
	$scope.isEditP = true;
	$scope.isEdit = true;
  $scope.isSave = false;
  $scope.isBackground = false;

  $scope.$watch(function(){
   //console.log(window.innerWidth);
   return window.innerWidth;
  }, function(value) {
     //console.log(value);
  }); 

  $scope.accountSave = function (data) {
    if (data.name && data.username && data.password && data.contact) {
      $scope.isProfile = !$scope.isProfile; 
      $scope.isEditP = !$scope.isEditP;
    }
  }
  $scope.profileSave = function (data) {
    if (data.name && data.email && data.contact[0].local && data.desc && data.address && data.deliveryTime && data.deliveryFee && data.deliverySched) {
      $scope.isEdit = !$scope.isEdit;
      $scope.isSave = !$scope.isSave;
    }
  }

	$http.get('assets/data/database.json')
    .then(function(response) {
    	console.log(response);
      if (response.status == 200) {
        $scope.profile = response.data.restaurant[0];
        console.log(response.data.restaurant[0].address);
      } else {
          // invalid response
      }

    	}, function(response) {
      // something went wrong
  	})
  $http.get('assets/data/database.json')
  .then(function(response) {
    console.log(response);
    if (response.status == 200) {
      $scope.account = response.data.account[0];
    } else {
        // invalid response
    }

    }, function(response) {
    // something went wrong
  })
})