angular.module('nav-controller',[])

.controller('NavCtrl', function ($scope, $rootScope, $state, $cookies, Authorization) {

	$scope.$on('$stateChangeSuccess',

  		function(event, toState, toParams, fromState, fromParams) {
		    $state.current = toState;
		    if ($state.current.name == 'app') {
		    	$rootScope.filter_status = '';
		    }
	  	}
	)

	$scope.logout = function () {
		$cookies.put('Authorization', false)
		$state.go("login");
	}
})