angular.module('orders-controller',[])

.controller('ordersCtrl', function ($scope, $rootScope, $state, $stateParams, $uibModal, $log, $sce, $http) {

  function init () {
    
    // Run this code only once
    console.log(!$stateParams.filter);
    if (!$stateParams.filter) {
      $state.go('orders', { filter:"All" });
      $scope.sum = 0;       
      console.log("Order Initialize");
      
      function http () {
        $http.get('assets/data/orders.json')
        .then(function(response) {
          if (response.status == 200) {
            console.log("Database GET status : " + response.status);
            console.log(response.data);
            $rootScope.orders = response.data.orders;
          } else {
            // invalid response
          }
        }, function(response) {
          // something went wrong
        });
      }
      http();
      console.log("reload DB");
    }
    // Run this code every Reload/Refresh
    if ($state.current.name == 'orders') {
      $rootScope.sidebar_switch = false;
      $rootScope.dashboard      = [ ];
      $rootScope.dashboard = [
        {"name" : "All"}, 
        {"name" : "New"}, 
        {"name" : "Delivered"}, 
        {"name" : "Canceled"}
      ];
      
    }
  };

  if($stateParams.filter == "All") 
    $scope.filter = "";
  else
    $scope.filter = $stateParams.filter;

  $scope.orderAccept = function (index) {

    $rootScope.index = index;
    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/orders/ordersAccept-modal.html',
      controller: 'accept',
      size: 'sm'
    })
  };
  $scope.orderDecline = function (index) {

    $rootScope.index = index;
    var modalInstance = $uibModal.open({
      templateUrl: 'app/components/orders/ordersDecline-modal.html',
      controller: 'decline',
      size: 'sm'
    })
  };
  init();
})

.controller('accept',  function ($scope, $rootScope, $uibModalInstance, $http, $stateParams) {

  $scope.filter = $stateParams.filter;

  $scope.ok = function (index, order) {
    console.log(order);
    if (order)
      $rootScope.orders[index].status = "Delivered";
    $uibModalInstance.dismiss('cancel');
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('decline', function ($scope, $rootScope, $uibModalInstance, $http, $stateParams) {

  $scope.filter = $stateParams.filter;

  $scope.ok = function (index, order) {
    console.log(order);
    if (order)
      $rootScope.orders[index].status = "Canceled";
    $uibModalInstance.dismiss('cancel');
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})
