var dependencies = [
	'ui.router',
	'ui.bootstrap',
	'ui.bootstrap.timepicker',
	'ui.bootstrap.dropdown',

	'ngMap',
	'ngCookies',
	'ngFileUpload',
	'ngAnimate',
	'highcharts-ng',

	'nav-controller',
	'sidebar-controller',
	'offers-controller',
	'orders-controller',
	'register-controller',
	'home-controller',
	'login-controller',
	'profile-controller',

	'http-service'
]

var app = angular.module('app',dependencies)

.filter('decodeURIComponent', function() {
	
	return window.decodeURIComponent;
})

//defines the routes on our website
.config(function($stateProvider, $urlRouterProvider){
	
	//default route
	$urlRouterProvider.otherwise("login"); 

	$stateProvider
	.state('app', {
		url 			: '/dashboard',		
		views			: {
			'topbar' 			: { templateUrl : 'app/shared/nav/nav.html',
									controller  : 'NavCtrl' },
			'content'			: { templateUrl : 'app/components/home/home.html',
									controller 	: 'homeCtrl' }
		},
		data: {
	      authorization: true,
	      redirectTo: 'login',
	      memory: true
	    }
	})

	.state('offers', {
		url 			: '/offers?filter',		
		views			: {
			'topbar' 			: { templateUrl : 'app/shared/nav/nav.html',
									controller  : 'NavCtrl' },
			'sidebar'          	: { templateUrl : 'app/shared/sidebar/sidebar.html',
									controller  : 'SidebarCtrl' },
			'content'			: { templateUrl : 'app/components/offers/offers.html',
									controller 	: 'offersCtrl' }
		},
		data: {
		      authorization: true,
		      redirectTo: 'login',
		      memory: true
	    }
	})

	.state('ordersTable', {
		url 			: '/-orders',		
		views			: {
			'topbar' 			: { templateUrl : 'app/shared/nav/nav.html',
									controller  : 'NavCtrl' },
			'sidebar'          	: { templateUrl : 'app/shared/sidebar/sidebar.html',
									controller  : 'SidebarCtrl' },
			'content'			: { templateUrl : 'app/components/orders/orders-table.html',
									controller 	: '' }
		}
	})

	.state('orders', {
		url 			: '/orders?filter',		
		views			: {
			'topbar' 			: { templateUrl : 'app/shared/nav/nav.html',
									controller  : 'NavCtrl' },
			'sidebar'          	: { templateUrl : 'app/shared/sidebar/sidebar.html',
									controller  : 'SidebarCtrl' },
			'content'			: { templateUrl : 'app/components/orders/orders.html',
									controller 	: 'ordersCtrl' }
		},
		data: {
	      authorization: true,
	      redirectTo: 'login',
	      memory: true
	    }
	})

	.state('profile', {
		url 			: '/profile',		
		views			: {
			'topbar' 			: { templateUrl : 'app/shared/nav/nav.html',
									controller  : 'NavCtrl' },
			'content'			: { templateUrl : 'app/components/profile/profile.html',
									controller 	: 'profileCtrl'
								}
		},
		data: {
	      authorization: true,
	      redirectTo: 'login',
	      memory: true
	    }
	})

	.state('login', {
		url 			: '/login',
		views			: {
			'content'			: { templateUrl : 'app/components/login/login.html',
									controller 	: 'loginCtrl' 
								}
		},
      	data: {
      		authorization: false
      	}
	})

	.state('register', {
		url 			: '/register',
		views			: {
			'content'			: { templateUrl : 'app/components/register/register.html',
									controller  : 'registerCtrl' }
		},
		data: {
			authorization: false
		}
	})
})

.run(function($rootScope, $state, $cookies, Authorization) {

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if ($state.current.data.authorization) {
    	if ($cookies.get('Authorization') == "false") {
    		$state.go('login');
    		window.alert("Access Denied");
    	}
    }

    if (!Authorization.authorized) {
      if (Authorization.memorizedState && (!_.has(fromState, 'data.redirectTo') || toState.name !== fromState.data.redirectTo)) {
        Authorization.clear();
      }
      if (_.has(toState, 'data.authorization') && _.has(toState, 'data.redirectTo')) {
        if (_.has(toState, 'data.memory')) {
          Authorization.memorizedState = toState.name;
        }
        $state.go(toState.data.redirectTo);
      }
    }
  });

  $rootScope.onLogout = function() {
    Authorization.clear();
    $cookies.put("Authorization", 'false');
    $state.go('home');
  };
})

.service('Authorization', function($state) {

  this.authorized = false,
  this.memorizedState = null;

  var
  clear = function() {
    this.authorized = false;
    this.memorizedState = null;
  },

  go = function(fallback) {
    this.authorized = true;
    var targetState = this.memorizedState ? this.memorizedState : fallback;
    $state.go(targetState);
  };

  return {
    authorized: this.authorized,
    memorizedState: this.memorizedState,
    clear: clear,
    go: go
  };
})

.service('httpSrvc', function ($scope, $rootScope, $http) {

	this.sample = "sample http service";

})