angular.module('register-controller',[])

.controller('registerCtrl', function ($scope, $rootScope, $log, $state, $http) {

    $scope.btnBack = "Back";
    $scope.btnNext = "Next";

    $scope.isInfo    = false;
    $scope.isLogo    = true;
    $scope.isVerify  = true;

    $scope.next = function (data) {

        if ($scope.btnNext == "Create Account") {
            console.log(data);
            // $http.put(database, data)
            $state.go('login');
        }
        if (data.restName && data.firstName && data.lastName && data.email && data.password && data.address) {
            
            if($scope.isInfo && $scope.isVerify && data.photo) {
                $scope.isVerify = false;
                $scope.isLogo   = true;
                $scope.isInfo   = true;
                $scope.btnNext  = "Create Account";
            }
            if($scope.isLogo && $scope.isVerify) {
                $scope.isVerify = true;
                $scope.isLogo   = false;
                $scope.isInfo   = true;
            }
        }
    };

    $scope.back = function () {

        if($scope.isInfo && $scope.isVerify) {
            $scope.isVerify = true;
            $scope.isLogo   = true;
            $scope.isInfo   = false;
            $scope.btnNext  = "Next";
        }
        if($scope.isLogo && $scope.isInfo) {
            $scope.isVerify = true;
            $scope.isLogo   = false;
            $scope.isInfo   = true;
            $scope.btnNext  = "Next";
        }
    }
});