angular.module('home-controller', [])

.controller('homeCtrl', function ($scope, $rootScope, $http) {
    
    $scope.salesChart;
    $http.get('assets/data/sales.json')
    .then(function(response) {
      if (typeof response.data === 'object') {
        $scope.salesChart = response.data;
      } else {
          // invalid response
      }

      }, function(response) {
        // something went wrong
    });

    $scope.offersChart;
    $http.get('assets/data/offer.json')
    .then(function(response) {
      if (typeof response.data === 'object') {
        $scope.offersChart = response.data;
      } else {
          // invalid response
      }

      }, function(response) {
        // something went wrong
    });
})
